import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/Http';
import { Observable } from 'rxjs/internal/Observable';
import { Passenger } from './Passenger';

@Injectable()
export class PassangerService
{
    private url:string="http://localhost:8081/";
constructor(private http : HttpClient){}

getPassengerDetails() : Observable<Passenger[]>
{
return this.http.get<Passenger[]>(this.url);
}
}
