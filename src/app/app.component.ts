import { Component, OnInit } from '@angular/core';
import { PassangerService } from './passanger.service'
import { Passenger } from './Passenger';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'angular-spring-integration';
  passengerdel:Passenger[];
  constructor(private passengerService:PassangerService){}
 ngOnInit(): void {

  this.passengerService.getPassengerDetails()
  .subscribe((res=>{
    console.log(res);
 this.passengerdel=res;
  }),
  error=>{
    console.log(error);
  });
 
}

}
