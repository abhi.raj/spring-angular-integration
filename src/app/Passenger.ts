export class Passenger{
    private pnr:Number;
    private pname:string;
    private source:string;
    private destination:string;
    private journeydate?:Date
    constructor(pnr:number, pName:string,sour:string,des:string,jou:Date){
        this.pnr=pnr;
        this.pname=pName;
        this.source=sour;
        this.destination=des;
        this.journeydate=jou;
    }
}